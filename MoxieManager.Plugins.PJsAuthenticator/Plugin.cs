﻿using MoxieManager.Core;
using MoxieManager.Core.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using WebMatrix.WebData;

namespace MoxieManager.Plugins.PJsAuthenticator
{
    public class Plugin : IPlugin, IAuthenticator
    {
        public void Init()
        {
        }

        public string Name
        {
            get { return "PJsAuthenticator"; }
        }

        public bool Authenticate(User user)
        {
            if (WebSecurity.IsAuthenticated && Roles.GetRolesForUser().Intersect(new[] { "Admin", "CMSuser", "CMSusers" }).Any())
                return true;
            else
                return false;
        }
        /// <summary>
        ///   Returns a IPv4 address as a int value, useful for range checking. 
        /// </summary>
        /// <param name="ip">IP address string to convert.</param>
        /// <returns>IPv4 address</returns>
        private static long GetIpAddress(string ip)
        {
            var bytes = System.Net.IPAddress.Parse(ip).GetAddressBytes();
            Array.Reverse(bytes);
            return BitConverter.ToUInt32(bytes, 0);
        }
    }
}
